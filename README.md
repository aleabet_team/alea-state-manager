# Alea State Manager

A simple library for managing the inner state of the application. Ideal for storing flags and session-based configuration values.

Built-in support for localStorage API provides an easy way of storring values across multiple sessions.

---

#### **Table of contents**
* Installation
* Storing data to the state
* Getting data from the state
* Updating data in the state
* Printing data from the state
* Storing data to the memory
* Getting data from the memory
* Updating data in the memory
* Deleting data from the memory
* Printing data from the memory
* Subscribing to the changes of the attributes on the state and memory objects

---

## Installation


To include Alea State Manager, simply add it to your project with the script tag.


```html

     <script src="/location_of_the_file/index.js"></script>

```

Then initiate the state and store it to the variable on the window object.

```js

    window.state = new initiateState();

```

To set the initial state of the application we need to pass the object with initial properties to the initiateState() constructor.

```js

    window.state = new initiateState({ language: 'en', theme: 'material-design', posts: 13 });

```

---

## Storing data to the state


To store the data to our state object we need to provide key and value pair to the .set() method

```js

    state.set('favoritAnimal', 'dog');

```

Type of the first argument provided to the .set() method needs to be a string. Type of the second argument can be anything except 'undefined'.

```js

    state.set('someString', 'Hello World');
    state.set('someNumber', 55);
    state.set('someBolean', false);
    state.set('someObject', { error: 500});
    state.set('someArray', [ 1, 2, 3 ]);
    state.set('someFunction', () => { alert('Hello World') });

```

To store multiple values to the state at once, we need to provide an object with the key-value pairs to the .set() method.

```js

    state.set({ name: 'Sasa', age: 19, country: 'Uganda'  });

```

NOTE: This will not add the entire provided object to the state. For each property in the provided object, property with the same name and the same value will be created on the state object.

---

## Getting data from the state


To get the data from the state, all we need to do is to provide the name of the attribute on the state object to the .get() method.

```js

   var name = state.get('name');
	//RESULT = "Sasa"

```

If we call the .get() method without any arguments, as result we will get the entire state object.

```js

   var name = state.get();
   //RESULT = ENTIRE STATE OBJECT

```

To fetch the array of the values from the state object, we need to provide all attribute names we want to fetch in the exact order, to the .getArray() method.

```js

   var arr = state.getArray('country', 'name', 'age');
	//RESULT = ["Uganda", "Sasa", 19]

```

---

To fetch an object containing multiple values from the state object, we need to provide all attribute names we want to fetch to the .getObject() method.

```js

   var obj = state.getObject('name', 'age', 'country');
	//RESULT = { name: 'Sasa', age: 19, country: 'Uganda'  }

```

---

## Updating data in the state

.update() method provides us with the easy way of updating properties on the state object based on their current value.
The method provides the current value of the attribute to the callback function. Whatever that callback function returns it will be set as the new value.

```js

	state.update('name', (prevName) => {
		return prevName + 'aaa...';
	});
	//RESULT = "Sasaaaa..."

```

This method is also useful for updating of the objects stored on the state object.

```js

   state.set('someObject', { val1: 500, val2 = 404});
   //RESULT = { val1: 500, val2 = 404}


	state.update('someObject', (prevObject) => {
		prevObject.val2 = prevObject.val2 * 2;
		prevObject.val3 = 600;
		return prevObject;
	});
	//RESULT = { val1: 500, val2 = 808, val3 = 600}

```

---

## Printing data from the state

.print() method on the state object provides us with a way to pretty-print our entire state object in the purposes of debugging.

```js

   console.log(state.print());
   //RESULT = { name: 'Sasa', age: 19, country: 'Uganda'  }

```

---

## Storing data to the memory


To store the data to our memory object we need to provide key and value pair to the .save() method

```js

    state.save('favoritAnimal', 'dog');

```

Type of the first argument provided to the .save() method needs to be a string. Type of the second argument can be anything except 'undefined'.

```js

    state.save('someString', 'Hello World');
    state.save('someNumber', 55);
    state.save('someBolean', false);
    state.save('someObject', { error: 500});
    state.save('someArray', [ 1, 2, 3 ]);
    state.save('someFunction', () => { alert('Hello World') });

```

To store multiple values to the memory at once, we need to provide an object with the key-value pairs to the .save() method.

```js

    state.save({ name: 'Sasa', age: 19, country: 'Uganda'  });

```

NOTE: This will not add the entire provided object to the memory. For each property in the provided object, property with the same name and the same value will be created on the memory object.

---

## Getting data from the memory


To get the data from the memory, all we need to do is to provide the name of the attribute on the state object to the .load() method.

```js

   var name = state.load('name');
	//RESULT = "Sasa"

```

If we call the .load() method without any arguments, as result we will get the entire memory object.

```js

   var name = state.load();
   //RESULT = ENTIRE MEMORY OBJECT

```

To fetch the array of the values from the memory object, we need to provide all attribute names we want to fetch in the exact order, to the .loadArray() method.

```js

   var arr = state.loadArray('country', 'name', 'age');
   //RESULT = ["Uganda", "Sasa", 19]

```

---

To fetch an object containing multiple values from the memory object, we need to provide all attribute names we want to fetch to the .loadObject() method.

```js

   var obj = state.loadObject('name', 'age', 'country');
   //RESULT = { name: 'Sasa', age: 19, country: 'Uganda'  }

```

---

## Deleting data from the memory

To delete the property from the memory object, we need to provide the property name to the .delete() method.

```js

   state.delete('name');
   //RESULT = 'name' attribute is deleted from the memory object

```

If we call .delete() method without any arguments, entire memory object will be deleted.

```js

   state.delete();
   //RESULT = memory object is completely empty

```

---

## Updating data in the memory

.updateSaved() method provides us with the easy way of updating properties on the memory object based on their current value.
The method provides the current value of the attribute to the callback function. Whatever that callback function returns it will be set as the new value.

```js

	state.updateSaved('name', (prevName) => {
		return prevName + 'aaa...';
	});
	//RESULT = "Sasaaaa..."

```

This method is also useful for updating of the objects stored on the state object.

```js

   state.save('someObject', { val1: 500, val2 = 404});
	//RESULT = { val1: 500, val2 = 404}


	state.updateSaved('someObject', (prevObject) => {
		prevObject.val2 = prevObject.val2 * 2;
		prevObject.val3 = 600;
		return prevObject;
	});
	//RESULT = { val1: 500, val2 = 808, val3 = 600}

```

---

## Printing data from the memory

.printSaved() method on the state object provides us with a way to pretty-print our entire memory object in the purposes of debugging.

```js

   console.log(state.printSaved());
   //RESULT = { name: 'Sasa', age: 19, country: 'Uganda'  }

```

---

## Subscribing to the changes of the attributes on the state and memory objects

Alea State Manager has built-in option for subscribing to the changes of the attributes on the state and memory object thanks to the .watch() method.
The method takes the attribute name and the callback function which will run each time that attribute changes.
Each time callback function runs, a new value of the attribute will be passed to it as the only argument.

```js

	state.watch('name', (newName) => {
		console.log('Name has changed, new name is ' + newName);
	});
	
	state.set('name', 'John Wayne');
	//RESULT = the next message will be logged to the console:
	// Name has changed, new name is John Wayne

```

The example below demonstrates how Alea State Manager can be used with AngularJS to set and change the variable in the scope.

```js

	$scope.name = state.get('name');

	state.watch('name', (newName) => {
		$scope.name =  newName;
	});

```

We can stop watching for changes of the specific attribute at any time by using .stop() method.

```js

	var watchForNameChanges = state.watch('name', (newName) => {
		console.log(newName);
	});
	//RESULT = We subscribed to the change event of the name attribute

	watchForNameChanges.stop();
	//RESULT = We unsubscribed from the change event of the name attribute

```

NOTE: .watch() method can be used both on the state and the memory object.

```js

	state.watch('email', (newEmail) => {
		console.log('Email has changed, new email is ' + newEmail);
	});
	
	state.save('email', 'contact@aleacontrol.com');
	//RESULT = the next message will be logged to the console:
	// Email has changed, new email is contact@aleacontrol.com

```