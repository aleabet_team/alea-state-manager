// Alea Control State constructor
(function () {
	// Run the next code in the strict mode
	"use strict";

   // CONSTRUCTOR START @@
   // --------------------------------------------
   window.initiateState = function (initialState) {
      // If initialState argument is provided, check is it an object and if not throw an error
      if (initialState && typeof initialState !== 'object') {
         throw new Error('If argument is provided, initiateState() constructor expects that argument to be an object '
            + 'representing the initial state. Instead ' + typeof initialState + ' ' + initialState + ' was provided.');
      };
      // If initialState is provided set it as the state object,
      // otherwise set an emty object as the state object
      this.__state__ = initialState || {};

      // Fetch the object from the local storage and if it exists set it 
      // as the memory object, otherwise set an emty object as the state object
      let saved = localStorage.getItem('__memory__');
      this.__memory__ = JSON.parse(saved) || {};

      // Holds all listeners for changed properties
      this.__subscriptions__ = {};
   };
   // --------------------------------------------
   // CONSTRUCTOR END @@




   // REUSABLE FUNCTIONS START @@
   // --------------------------------------------
   // Check is value an object and if it is returns it's copy
   // Check is value an array and if it is returns it's copy
   // If value is primitive value, return it
   const returnValue = (value) => {
      if (typeof value === 'object' && value !== null) {
         if (!Array.isArray(value)) {
            return Object.assign({}, value);
         };
         return Array.prototype.concat([], value);
      };
      return value;
   };

   // Function takes target object and arguments list passed from the outer function
   // If arguments list is not emty, get the value for each argument from the state
   // and push it into temporary array (tempArr)
   // On the end, return tempArr or empty array if arguments list is empty
   const returnArray = (object, args) => {
      if (args.length > 0) {
         let tempArr = [];
         for (let i = 0; i < args.length; i++) {
            tempArr.push(object[args[i]]);
         };
         return tempArr;
      };
      return [];
   };

   // Function takes target object and arguments list passed from the outer function
   // If arguments list is not emty, get the value for each argument from the state
   // and add it to the  temporary object (tempObj)
   // On the end, return tempObj or an empty object if arguments list is empty
   const returnObject = (object, args) => {
      if (args.length > 0) {
         let tempObj = {};
         for (let i = 0; i < args.length; i++) {
            tempObj[args[i]] = object[args[i]];
         };
         return tempObj;
      };
      return {};
   };

   // Function takes argument and checks is that argument undefined and if is it returns false.
   // If argument is null, 0, '' or false, function will return true
   const isThere = (val) => {
      if (val || val === null || val === '' || val === 0 || val === false)
         return true;
      return false;
   };
   // --------------------------------------------
   // REUSABLE FUNCTIONS END @@





   // STATE METHODS START @@
   // --------------------------------------------
   // Method for setting properties on the state object -->
   initiateState.prototype.set = function (prop, value) {
      // Error handling --->
      if (!prop) {
         throw new Error('.set() method expects at least one argument.');
      };
      if (!isThere(value) && (prop === null || typeof prop !== 'object')) {
         throw new Error('If only one argument is provided, .set() method expects '
            + 'that argument to be an object, but instead ' + typeof prop + ' "' + prop + '" was provided.');
      };
      if (isThere(prop) && value && typeof prop !== 'string') {
         throw new Error('When two properties are provided, .set() method expects the first argument to be a string '
            + 'representign argument name on the state object Instead ' + typeof prop + ' ' + prop + ' was provied.');
      };
      if (isThere(this.__memory__[prop])) {
         throw new Error('DUPLICATE PROPERTY: This argument already exists in the memory, '
            + 'you cannot have the same properties on the state object and saved to the memory.');
      };
      // <--- Error handling

      // IF only one argument is provided, and that argument is an object,
      // for each property on that object, set that property on the state object.
      // Emit an event for each of those properties so event listeners can recieve update 
      // (if they are subscribed to this property). On the end, return the provided object
      // ---
      // ELSE if both prop and value argumets are provided, set the property 'prop' on the
      // state object and give it a value of 'val'. Emit an event for that property so 
      // event listeners can recieve update. On the end, return the provided value
      if (!value && prop !== null && typeof prop === 'object') {
         for (let key in prop) {
            this.__state__[key] = prop[key];
            this.execute(key, prop[key]);
         };
         return prop;
      } else {
         this.__state__[prop] = value;
         this.execute(prop, value);
         return value;
      };
   }; // <-- Method for setting properties on the state object 

   // Method for updating of the existing properties on the state object -->
   initiateState.prototype.update = function (prop, fn) {
      // Error handling --->
      if (!prop || !fn || typeof prop !== 'string' || typeof fn !== 'function') {
         throw new Error('.update() method expects first argument to be a string representing '
            + 'property for modification, and second property to be a function for modifying that property.');
      };
      if (!isThere(this.__state__[prop])) {
         throw new Error('Property ' + prop + ' doesnt exists on the state object.');
      };
      // <--- Error handling

      // Set the update variable to the value returned by the callback function
      // If returned value is not undefined, save the changes to the state object and
      // emit an event for that property so event listeners can recieve update.
      let update = fn(returnValue(this.__state__[prop]));
      if (isThere(update)) {
         this.__state__[prop] = update;
         this.execute(prop, update);
         return update;
      };
   }; // <-- Method for updating of the existing properties on the state object

   // Method for fetching the property from the state object -->
   // If prop argument is provided and that argument is a string
   // return the property from the state, if not return entire state object
   initiateState.prototype.get = function (prop) {
      if (prop) {
         if (typeof prop !== 'string') {
            throw new Error('If argument is provided, .get() method expects that argument to be a string'
               + 'representing property on the state object, you provided ' + typeof prop + ' ' + prop + ' instead.');
         };
         return returnValue(this.__state__[prop]);
      };
      return returnValue(this.__state__);
   }; // <-- Method for fetching the property from the state object

   // Method for creating an array using multiple properties from the state object -->
   initiateState.prototype.getArray = function () {
      return returnArray(this.__state__, arguments);
   }; // <-- Method for creating an array using multiple properties from the state object

   // Method for creating an object using multiple properties from the state object -->
   initiateState.prototype.getObject = function () {
      return returnObject(this.__state__, arguments);
   }; // <-- Method for creating an object using multiple properties from the state object

   // Method for printing formated string representing entire state object -->
   initiateState.prototype.print = function () {
      return JSON.stringify(this.__state__, null, 2);
   }; // <-- Method for printing formated string representing entire state object
   // --------------------------------------------
   // STATE METHODS END @@




   // MEMORY METHODS START @@
   // --------------------------------------------
   // Method for saving properties to the memory -->
   initiateState.prototype.save = function (prop, value) {
      // Error handling --->
      if (!prop) {
         throw new Error('.save() method expects at least one argument.');
      };
      if (!isThere(value) && (prop === null || typeof prop !== 'object')) {
         throw new Error('If only one argument is provided, .save() method expects '
            + 'that argument to be an object, but instead ' + typeof prop + ' "' + prop + '" was provided.');
      };
      if (isThere(prop) && value && typeof prop !== 'string') {
         throw new Error('When two properties are provided, .save() method expects the first argument to be a string '
            + 'representign argument name in the memory object Instead ' + typeof prop + ' ' + prop + ' was provied.');
      };
      if (isThere(this.__state__[prop])) {
         throw new Error('DUPLICATE PROPERTY: This argument already exists on the state object, '
            + 'you cannot have the same properties on the state object and saved to the memory.');
      };
      // <--- Error handling

      // IF only one argument is provided, and that argument is an object,
      // for each property on that object, set that property on the memory object.
      // Emit an event for each of those properties so event listeners can recieve update 
      // (if they are subscribed to this property). On the end, return the provided object
      // ---
      // ELSE if both prop and value argumets are provided, set the property 'prop' on the
      // memory object and give it a value of 'val'. Emit an event for that property so 
      // event listeners can recieve update. On the end, return the provided value
      // ---
      // In both cases, turn memory object into string and store it to the local storage
      if (!value && prop !== null && typeof prop === 'object') {
         for (let key in prop) {
            this.__memory__[key] = prop[key];
            this.execute(key, prop[key]);
         };
         return prop;
      } else {
         this.__memory__[prop] = value;
         this.execute(prop, value);
         return value;
      };
      let objString = JSON.stringify(this.__memory__);
      localStorage.setItem('__memory__', objString);
   }; // <-- Method for saving properties to the memory

   // Method for updating of the existing properties saved to the memory -->
   initiateState.prototype.updateSaved = function (prop, fn) {
      // Error handling --->
      if (!prop || !fn || typeof prop !== 'string' || typeof fn !== 'function') {
         throw new Error('.updateSaved() method expects first argument to be a string representing '
            + 'property for modification, and second property to be a function for modifying that property.');
      };
      if (!isThere(this.__memory__[prop])) {
         throw new Error('Property ' + prop + ' doesnt exists on the state object.');
      };
      // <--- Error handling

      // Set the update variable to the value returned by the callback function
      // If returned value is not undefined, save the changes to the state object and
      // emit an event for that property so event listeners can recieve update.
      // Convert memory object into string and save it to the local storage
      let update = fn(returnValue(this.__memory__[prop]));
      if (isThere(update)) {
         this.__memory__[prop] = update;
         this.execute(prop, update);

         let objString = JSON.stringify(this.__memory__);
         localStorage.setItem('__memory__', objString);
         return update;
      };
   }; // <-- Method for updating of the existing properties saved to the memory

   // Method for fetching the property from the memory object -->
   // If prop argument is provided and that argument is a string
   // return the property from the memory, if not return entire memory object
   initiateState.prototype.load = function (prop) {
      if (prop) {
         if (typeof prop !== 'string') {
            throw new Error('If argument is provided, .load() method expects that argument to be a string'
               + 'representing property on the state object, you provided ' + typeof prop + ' ' + prop + ' instead.');
         };
         return returnValue(this.__memory__[prop]);
      };
      return returnValue(this.__memory__);
   }; // <-- Method for fetching property from the memory object

   // Method for creating an array using multiple properties memory object -->
   initiateState.prototype.loadArray = function () {
      return returnArray(this.__memory__, arguments);
   }; // <-- Method for creating an array using multiple properties memory object

   // Method for creating an object using multiple properties memory object -->
   initiateState.prototype.loadObject = function () {
      return returnObject(this.__memory__, arguments);
   }; // <-- Method for creating an object using multiple properties memory object


   // Method for deleting property from the memory object -->
   // If prop argument is provided, type of that argument is string and that property exists in the memory,
   // delete that property from the memory. Otherwise throw an error. After that emit the change event
   //  for the provided property. If prop argument is not provided, empty entire memory object.
   // In both cases, save updated memory object to the local storage
   initiateState.prototype.delete = function (prop) {
      if (prop) {
         if (typeof prop !== 'string') {
            throw new Error('.delete() method expects only provided argument to be a string representing the '
               + 'property on the memory object. You provided ' + typeof prop + ' ' + prop + ' instead.');
         };
         if (!this.__memory__[prop]) {
            throw new Error('Property ' + prop + ' does not exist in the memory.');
         };

         delete this.__memory__[prop];
         this.execute(prop, null);
      } else {
         this.__memory__ = {};
      };

      let objString = JSON.stringify(this.__memory__);
      localStorage.setItem('__memory__', objString);
   }; // <-- Method for deleting property from the memory object

   // Method for printing formated string representing entire memory object -->
   initiateState.prototype.printSaved = function () {
      return JSON.stringify(this.__memory__, null, 2);
   }; // <-- Method for printing formated string representing entire memory object
   // --------------------------------------------
   // MEMORY METHODS START @@




   // SUBSCRIPTION METHODS START @@
   // --------------------------------------------
   // Event listener method for subscribing to the property change events -->
   // If subscription array for this property already exists, push the callback function to that array,
   // if it doesn't exist, create a new array and push the callback function to that array
   // On the end return the stop (incharge of canceling subscriptions) from the .watch() method
   initiateState.prototype.watch = function (prop, fn) {
      if (!prop || !fn || typeof prop !== 'string' || typeof fn !== 'function') {
         throw new Error('.watch() method expects the first argument to be a string representing the property to watch '
            + 'and second argument to be a callback function which will run each time provided property is changed.');
      };

      let self = this;
      if (this.__subscriptions__[prop]) {
         this.__subscriptions__[prop].push(fn);
      } else {
         this.__subscriptions__[prop] = [];
         this.__subscriptions__[prop].push(fn);
      };

      return {
         stop: function () {
            self.cancelSubscription(prop, fn);
         }
      };
   }; // <-- Event listener method for subscribing to the property change events

   // Method for executing callback functions for the property change listeners -->
   // If subscription array for property exists, pass the new value for 
   // that property to each callback function in the array and invoke it
   initiateState.prototype.execute = function (prop, newState) {
      if (this.__subscriptions__[prop]) {
         let functions = this.__subscriptions__[prop],
            index = 0,
            length = functions.length;

         for (; index < length; index++) {
            functions[index](newState);
         };
      };
   }; // <-- Method for executing callback functions for the property change listeners

   // Method for canceling subscriptions for the property change listeners -->
   // If subscription array for property exists, compare each function in the array
   // with the function provided, if match is found, remove that function from hte array
   initiateState.prototype.cancelSubscription = function (prop, fn) {
      if (this.__subscriptions__[prop]) {
         let functions = this.__subscriptions__[prop],
            newFunctions = [],
            index = 0,
            length = functions.length;

         for (; index < length; index++) {
            if (functions[index] !== fn) {
               newFunctions.push(functions[index]);
            };
         };

         if (newFunctions.length < length) {
            this.__subscriptions__[prop] = newFunctions;
         };
      };
   }; // <-- Method for canceling subscriptions for the property change listeners
   // --------------------------------------------
   // SUBSCRIPTION METHODS END @@
})();